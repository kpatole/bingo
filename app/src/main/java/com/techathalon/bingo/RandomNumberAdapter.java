package com.techathalon.bingo;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class RandomNumberAdapter extends RecyclerView.Adapter<RandomNumberAdapter.RandomViewHolder> {
    List<Integer> generated;
    Context context;

    public RandomNumberAdapter(Context context, List<Integer> generated) {
        this.generated = generated;
        this.context = context;
    }

    @NonNull
    @Override
    public RandomViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.random_number, null, false);
        return new RandomViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull RandomViewHolder viewHolder, int i) {

        viewHolder.number.setText(""+generated.get(i));
        Log.i("Random", ""+generated.get(i));

    }

    @Override
    public int getItemCount() {
        return generated.size();
    }
    public class RandomViewHolder extends RecyclerView.ViewHolder {
        TextView number;
        public RandomViewHolder(@NonNull View itemView) {
            super(itemView);
            number = itemView.findViewById(R.id.random_number);
        }
    }
}
