package com.techathalon.bingo;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

public class BingoAdapter extends RecyclerView.Adapter<BingoAdapter.BingoViewHolder> {
    Context context;
    List<NumberModel> numbers;
    List<Integer> generated;
    Integer next;

    public BingoAdapter(Context context, List<NumberModel> numbers, List<Integer> generated) {
        this.context = context;
        this.numbers = numbers;
        this.generated = generated;
    }

    public BingoAdapter(Context context, List<NumberModel> numbers) {
        this.context = context;
        this.numbers = numbers;
    }

    @NonNull
    @Override
    public BingoViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.number_layout, null, false);
        RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        view.setLayoutParams(lp);
//        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//        params.height =80; //height recycleviewer
//        view.setLayoutParams(params);
        return new BingoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BingoViewHolder bingoViewHolder, int i) {



        NumberModel list = numbers.get(i);
        if (generated != null) {

            if (generated.contains(list.getNumber())) {
                GradientDrawable shape = new GradientDrawable();
                shape.setShape(GradientDrawable.RECTANGLE);
                shape.setColor(Color.RED);
                shape.setStroke(3, Color.YELLOW);
//                bingoViewHolder.textView.setBackgroundColor(context.getResources().getColor(R.color.colorAccent));
                bingoViewHolder.textView.setBackgroundDrawable(shape);
                bingoViewHolder.textView.setText("" + list.getNumber());
            } else {
                bingoViewHolder.textView.setText("" + list.getNumber());
            }
        } else {
            bingoViewHolder.textView.setText("" + list.getNumber());
        }
    }


    @Override
    public int getItemCount() {
        return numbers.size();
    }

    public class BingoViewHolder extends RecyclerView.ViewHolder {
        TextView textView;
        Button button;

        public BingoViewHolder(@NonNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.number);
        }

    }
}
