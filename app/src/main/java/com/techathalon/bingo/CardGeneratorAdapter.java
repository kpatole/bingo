package com.techathalon.bingo;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

class CardGeneratorAdapter extends RecyclerView.Adapter<CardGeneratorAdapter.CardViewHolder> {
    Context context;

    List<Integer> generated;
    List<Integer> generateCardNum;
    Integer next;

    public CardGeneratorAdapter(Context context, List<Integer> generated) {
        this.context = context;
        this.generated = generated;
    }

    public CardGeneratorAdapter(Context context, List<Integer> generated, List<Integer> generateCardNum) {
        this.context = context;
        this.generated = generated;
        this.generateCardNum = generateCardNum;
    }

    @NonNull
    @Override
    public CardViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.number_layout, null, false);
        return new CardViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CardViewHolder cardViewHolder, int i) {
        Integer integer = generated.get(i);
        if (generateCardNum != null) {
            if (generateCardNum.contains(generated.get(i))) {
                GradientDrawable shape = new GradientDrawable();
                shape.setShape(GradientDrawable.RECTANGLE);
                shape.setColor(Color.RED);
                shape.setStroke(3, Color.YELLOW);
                cardViewHolder.textView.setText("" + generated.get(i));
                cardViewHolder.textView.setBackgroundDrawable(shape);
            }
            else {
                cardViewHolder.textView.setText("" + generated.get(i));
            }
        }
        else {
            cardViewHolder.textView.setText("" + generated.get(i));
        }


    }

    @Override
    public int getItemCount() {
//        Collections.sort(generated);
//        if(generated.size()>25)
//            return 5;
//        else
        return generated.size();
    }

    public class CardViewHolder extends RecyclerView.ViewHolder {
        TextView textView;

        public CardViewHolder(View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.number);
        }
    }
}
