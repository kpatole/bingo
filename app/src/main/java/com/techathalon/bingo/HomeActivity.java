package com.techathalon.bingo;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener {

    Button bingoCaller, cardGenerator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        bingoCaller = findViewById(R.id.bingoCaller);
        bingoCaller.setOnClickListener(this);
        cardGenerator = findViewById(R.id.cardGenerator);
        cardGenerator.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.bingoCaller:
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                break;
            case R.id.cardGenerator:
                Intent intent1 = new Intent(this, GenerateCard.class);
                startActivity(intent1);
                break;
        }
    }
}
