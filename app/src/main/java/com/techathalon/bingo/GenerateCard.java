package com.techathalon.bingo;

import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class GenerateCard extends AppCompatActivity {

    Timer timeoutTimer;
    GenerateTask genTask;

    RecyclerView recyclerView, randomRecyclerView;
    StaggeredGridLayoutManager manager;
    CardGeneratorAdapter adapter;
    RandomNumberAdapter adapter2;
    GradientDrawable shape;
    Button play;
    TextView b, i, n, g, o;
    List<Integer> cardNumbers1, position, generated;
    LinearLayoutManager layoutManager;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generate_card);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        genTask = new GenerateTask();

        recyclerView = findViewById(R.id.recyclerView);
        randomRecyclerView = findViewById(R.id.randomRecyclerView);

        position = new ArrayList<>();

        shape = new GradientDrawable();

        b = findViewById(R.id.b);
        i = findViewById(R.id.i);
        n = findViewById(R.id.n);
        g = findViewById(R.id.g);
        o = findViewById(R.id.o);

        play = findViewById(R.id.bingoButton);
        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//               generateRandomNumber();
                if(!genTask.started){
                    genTask.started=true;
                    timeoutTimer = new Timer();
                    timeoutTimer.scheduleAtFixedRate(genTask, 0, 3000);
                }
                else {
                    genTask.started=false;
                    timeoutTimer.cancel();
                }
            }
        });

        shape.setShape(GradientDrawable.OVAL);
        shape.setColor(Color.GREEN);
        shape.setStroke(3, Color.RED);
        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        manager = new StaggeredGridLayoutManager(5, StaggeredGridLayoutManager.VERTICAL);
        adapter = new CardGeneratorAdapter(this, generateCardNum());
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(adapter);
    }

    private void generateRandomNumber() {
        Random rng = new Random();
        Integer next = rng.nextInt(75) + 1;
        if (!generated.contains(next)) {
            Log.i("Random", next.toString());
            generated.add(next);
            adapter = new CardGeneratorAdapter(GenerateCard.this, position, generated);
            recyclerView.setNestedScrollingEnabled(false);
            recyclerView.setLayoutManager(manager);
            recyclerView.setAdapter(adapter);

            adapter2 = new RandomNumberAdapter(GenerateCard.this, generated);
            randomRecyclerView.setLayoutManager(layoutManager);
            randomRecyclerView.setNestedScrollingEnabled(false);
            layoutManager.scrollToPosition(generated.size() - 1);
            randomRecyclerView.setAdapter(adapter2);
            changeTextColor(next);

        } else {
            generateRandomNumber();

        }
    }

    private void changeTextColor(Integer next) {

    }

    private List<Integer> generateCardNum() {
        cardNumbers1 = new ArrayList<>();
        Random rng = new Random();
        int k = 5;
        for (int j = 1; j <= 75; j += 15) {
            do {
                Integer next = rng.nextInt(15) + j;
                if (!cardNumbers1.contains(next)) {
                    cardNumbers1.add(next);
                }
            } while (cardNumbers1.size() != k);
            k += 5;
        }
        for (int i = 0; i < 5; i++) {

            position.add(cardNumbers1.get(i));
            position.add(cardNumbers1.get(i + 5));
            position.add(cardNumbers1.get(i + 10));
            position.add(cardNumbers1.get(i + 15));
            position.add(cardNumbers1.get(i + 20));


        }
        checkBingo(position, cardNumbers1);
        return position;
    }

    private void checkBingo(List<Integer> position, List<Integer> cardNumbers1) {
        for(int i = 0 ;i<5; i++){
            if(position.get(i) == cardNumbers1.get(i) && position.get(i+5) == cardNumbers1.get(i+5) && position.get(i+10) == cardNumbers1.get(i+10)
            && position.get(i+15) == cardNumbers1.get(i+15)&& position.get(i+20) == cardNumbers1.get(i+20)){

            }
        }
    }

    private class GenerateTask extends TimerTask {
      List  generated = new ArrayList<>();
        boolean started = false;
        Random rng = new Random();

        @Override
        public void run() {
            if(started){
                Integer next = rng.nextInt(75)+1;
                if(!generated.contains(next)) {
                            generated.add(next);
                            changeTextColor(next);
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        adapter = new CardGeneratorAdapter(GenerateCard.this, position, generated);
                        recyclerView.setNestedScrollingEnabled(false);
                        recyclerView.setLayoutManager(manager);
                        recyclerView.setAdapter(adapter);

                        adapter2 = new RandomNumberAdapter(GenerateCard.this, generated);
                        randomRecyclerView.setLayoutManager(layoutManager);
                        randomRecyclerView.setNestedScrollingEnabled(false);
                        layoutManager.scrollToPosition(generated.size() - 1);
                        randomRecyclerView.setAdapter(adapter2);

                    }
                });
            }
        }
    }
}
