package com.techathalon.bingo;

import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    BingoAdapter adapter;
    GridLayoutManager manager;
    RecyclerView recyclerView;
    Button play, reset;
    List<Integer> generated;
    List<NumberModel> bingoNumbers;
    TextView t1, t2, t3, t4, t5, randomNum;
    List<Integer> B;
    List<Integer> I;
    List<Integer> N;
    List<Integer> G;
    List<Integer> O;
    GradientDrawable shape;
    GradientDrawable shape1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.recyclerView);
//        generated
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        manager = new GridLayoutManager(this, 15);
        adapter = new BingoAdapter(this, getNumbers());
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(adapter);
        reset = findViewById(R.id.reset);
        play = findViewById(R.id.play);
        t1 = findViewById(R.id.bingoText1);
        t2 = findViewById(R.id.bingoText2);
        t3 = findViewById(R.id.bingoText3);
        t4 = findViewById(R.id.bingoText4);
        t5 = findViewById(R.id.bingoText5);
        randomNum = findViewById(R.id.randomNumber);
        generated = new ArrayList<>();
        B = new ArrayList<>();
        I = new ArrayList<>();
        N = new ArrayList<>();
        G = new ArrayList<>();
        O = new ArrayList<>();

        shape1 = new GradientDrawable();
        shape1.setShape(GradientDrawable.OVAL);
        shape1.setColor(Color.CYAN);
        shape1.setStroke(3, Color.RED);
        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                generateRandomNumber();


//                    if(!B.contains(next)) {
//                        if (next >= 1 && next <= 15) {
//                            B.add(next);
//                            if (B.size() == 15) {
//                                t1.setBackgroundColor(Color.GREEN);
//                            }
//                        }
//                    }
//                    if(!I.contains(next)) {
//                        if (next >= 16 && next <= 30) {
//                            I.add(next);
//                            if (I.size() == 15) {
//                                t2.setBackgroundColor(Color.GREEN);
//                            }
//                        }
//                    }
//                    if(!N.contains(next)) {
//                        if (next >= 31 && next <= 45) {
//                            N.add(next);
//                            if (N.size() == 15) {
//                                t3.setBackgroundColor(Color.GREEN);
//                            }
//                        }
//                    }
//                    if(!G.contains(next)) {
//                        if (next >= 46 && next <= 60) {
//                            G.add(next);
//                            if (G.size() == 15) {
//                                t4.setBackgroundColor(Color.GREEN);
//                            }
//                        }
//                    }
//                    if(!O.contains(next)) {
//                        if (next >= 61 && next <= 75) {
//                            O.add(next);
//                            if (O.size() == 15) {
//                                t5.setBackgroundColor(Color.GREEN);
//                            }
//                        }
//                    }


            }
        });

        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                adapter = new BingoAdapter(MainActivity.this, getNumbers());
                recyclerView.setLayoutManager(manager);
                recyclerView.setAdapter(adapter);
                recyclerView.setNestedScrollingEnabled(false);
                generated.clear();
                B.clear();
                I.clear();
                N.clear();
                G.clear();
                O.clear();
                t1.setBackgroundColor(Color.WHITE);
                t2.setBackgroundColor(Color.WHITE);
                t3.setBackgroundColor(Color.WHITE);
                t4.setBackgroundColor(Color.WHITE);
                t5.setBackgroundColor(Color.WHITE);
            }
        });


    }

    private void generateRandomNumber() {
        Random rng = new Random();
        Integer next = rng.nextInt(75) + 1;
        if (!generated.contains(next)) {
            Log.i("Random", next.toString());
            generated.add(next);
            adapter = new BingoAdapter(MainActivity.this, getNumbers(), generated);
            recyclerView.setNestedScrollingEnabled(false);
            recyclerView.setLayoutManager(manager);
            recyclerView.setAdapter(adapter);
            changeTextColor(next);
            randomNum.setText("" + next);
            randomNum.setBackgroundDrawable(shape1);

        } else {
            generateRandomNumber();

        }
    }

    private void changeTextColor(Integer next) {
        shape = new GradientDrawable();
        shape.setShape(GradientDrawable.RECTANGLE);
        shape.setColor(Color.GREEN);
        shape.setStroke(3, Color.RED);
        if (next >= 1 && next <= 15) {
            if (!B.contains(next)) {
                B.add(next);
                if (B.size() == 15) {
                    t1.setBackgroundDrawable(shape);
                }
            }
        } else if (next >= 16 && next <= 30) {
            if (!I.contains(next)) {
                I.add(next);
                if (I.size() == 15) {
                    t2.setBackgroundDrawable(shape);
                }
            }
        } else if (next >= 31 && next <= 45) {
            if (!N.contains(next)) {
                N.add(next);
                if (N.size() == 15) {
                    t3.setBackgroundDrawable(shape);
                }
            }
        } else if (next >= 46 && next <= 60) {
            if (!G.contains(next)) {
                G.add(next);
                if (G.size() == 15) {
                    t4.setBackgroundDrawable(shape);
                }
            }
        } else if (next >= 61 && next <= 75) {
            if (!O.contains(next)) {
                O.add(next);
                if (O.size() == 15) {
                    t5.setBackgroundDrawable(shape);
                }
            }
        }
    }

    private List<NumberModel> getNumbers() {
        bingoNumbers = new ArrayList<>();
        for (int i = 1; i <= 75; i++) {
            NumberModel list = new NumberModel();
            list.number = i;
            bingoNumbers.add(list);
        }
        return bingoNumbers;
    }
}
